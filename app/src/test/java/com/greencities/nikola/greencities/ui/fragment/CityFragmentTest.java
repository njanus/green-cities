package com.greencities.nikola.greencities.ui.fragment;

import android.content.Context;
import android.widget.Filter;

import com.greencities.nikola.greencities.adapter.CityListAdapter;
import com.greencities.nikola.greencities.model.City;

import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class CityFragmentTest {


    Filter filter;
    private List<City> cities;
    @Mock
    private Context context;
    private CityListAdapter.CityAdapterListener listener;

    @Test
    public void returnString_WhenSearchIsFound() throws Exception {
        cities = new ArrayList<>();
        City city1 = new City();
        city1.setCity("Berlin");
        City city2 = new City();
        city2.setCity("Belgrade");
        cities.add(city1);
        cities.add(city2);
        CityListAdapter adapter = new CityListAdapter(context, cities, listener);
        when(adapter.getFilter()).thenReturn(filter);
        //   assert (adapter.getFilter().filter("grade"));
    }
}