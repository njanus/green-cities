package com.greencities.nikola.greencities.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.greencities.nikola.greencities.enums.Status;

import java.io.Serializable;

@Entity(tableName = "City", indices = {@Index(value = {"city"},
        unique = true)})
public class City implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;

    @ColumnInfo
    private String city;

    @ColumnInfo(name = "cityAsc")
    private String cityAscii;

    @ColumnInfo(name = "latitude")
    private Double lat;

    @ColumnInfo(name = "longitude")
    private Double lng;

    @ColumnInfo(name = "pop")
    private Double pop;

    @ColumnInfo
    private String country;

    @ColumnInfo
    private String iso2;

    @ColumnInfo
    private String iso3;

    @ColumnInfo
    private String province;

    @ColumnInfo(name = "status")
    private int status;

    public City() {
        this.status = Status.UNDEFINED.getValue();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityAscii() {
        return cityAscii;
    }

    public void setCityAscii(String cityAscii) {
        this.cityAscii = cityAscii;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getPop() {
        return pop;
    }

    public void setPop(Double pop) {
        this.pop = pop;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }

    public String getIso3() {
        return iso3;
    }

    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "City{" +
                "city='" + city + '\'' +
                ", cityAscii='" + cityAscii + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", pop=" + pop +
                ", country='" + country + '\'' +
                ", iso2='" + iso2 + '\'' +
                ", iso3='" + iso3 + '\'' +
                ", province='" + province + '\'' +
                ", status=" + status +
                '}';
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }
}
