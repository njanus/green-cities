package com.greencities.nikola.greencities.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.greencities.nikola.greencities.R;
import com.greencities.nikola.greencities.model.City;

public class CityDetailsActivity extends AppCompatActivity {
    private static final String CITY_DATA = "city_data";
    private TextView textViewName;
    private TextView textViewCountry;
    private TextView textViewLtn;
    private TextView textViewLng;
    private TextView textViewProvince;
    private RadioButton radioButtonUndefined;
    private RadioButton radioButtonGreen;
    private Button btnUpdate;
    private City city;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_details);
        initView();
        getIntentData();
        setViewData();

    }

    private void setViewData() {
        if (city != null) {
            textViewCountry.setText(city.getCountry());
            textViewName.setText(city.getCity());
            if (city.getStatus() == 0) {
                radioButtonUndefined.setChecked(true);
            } else {
                radioButtonGreen.setChecked(true);
            }
            //TODO change this with place holders
            //this is wrong way of setting data and shoud be implemented with placeholders
            textViewLng.setText(textViewLng.getText() + " : " + city.getLng());
            textViewLtn.setText(textViewLtn.getText() + " : " + city.getLat());
            textViewProvince.setText(textViewProvince.getText() + " : " + city.getProvince());
        } else {
            Toast.makeText(this, "No city meatadata", Toast.LENGTH_LONG).show();
        }
    }

    private void getIntentData() {
        city = (City) getIntent().getSerializableExtra(CITY_DATA);
    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        radioButtonUndefined = findViewById(R.id.radioBtnUndefined);
        radioButtonGreen = findViewById(R.id.radioBtnGreen);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isGreen = false;
                if (radioButtonGreen.isChecked()) {
                    isGreen = true;
                }
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", isGreen);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
        textViewName = findViewById(R.id.textViewCityName);
        textViewCountry = findViewById(R.id.textViewCityCountry);
        textViewLtn = findViewById(R.id.textViewLatitude);
        textViewLng = findViewById(R.id.textViewLongitude);
        textViewProvince = findViewById(R.id.textViewProvince);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

