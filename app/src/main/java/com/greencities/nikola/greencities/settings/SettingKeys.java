package com.greencities.nikola.greencities.settings;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SettingKeys {

    public static final String FIRST_TIME_APP_STARTED = "app_first_init";
    public static final String CITY_STATUS = "city_value";


    @StringDef({
            FIRST_TIME_APP_STARTED,
            CITY_STATUS,
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface SettingKey {

    }
}
