package com.greencities.nikola.greencities.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.greencities.nikola.greencities.R;

public class SplashScreenActivity extends AppCompatActivity {

    private ImageView imageViewGif;
    private TextView textViewTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        initViews();
        setAnimation();
    }


    private void initViews() {
        imageViewGif = findViewById(R.id.imageViewAnimation);
        textViewTitle = findViewById(R.id.textViewTitle);
    }

    private void setAnimation() {
        TranslateAnimation tAnimation = new TranslateAnimation(0, 0, 270, 0);
        tAnimation.setDuration(2500);
        tAnimation.setStartTime(1000);
        tAnimation.setRepeatCount(0);
        tAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        tAnimation.setFillAfter(true);
        tAnimation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textViewTitle.setVisibility(View.VISIBLE);
                startNextMatchingActivity();

            }
        });

        imageViewGif.startAnimation(tAnimation);
        textViewTitle.startAnimation(tAnimation);
    }

    private void startNextMatchingActivity() {
        Intent mainIntent = new Intent(this, MainActivity.class);
        finish();
        startActivity(mainIntent);
    }
}