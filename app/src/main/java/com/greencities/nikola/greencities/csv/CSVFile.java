package com.greencities.nikola.greencities.csv;

import android.content.Context;

import com.greencities.nikola.greencities.model.City;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVFile {
    private InputStream inputStream;

    public CSVFile(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public List<City> readDataFromCSV(Context context) {
        List<City> cities = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            reader.readLine();
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] record = csvLine.split(";");
                int size = record.length;
                City city = new City();
                city.setCity(record[0]);
                city.setCityAscii(record[1]);
                city.setLat(Double.valueOf(record[2]));
                city.setLng(Double.valueOf(record[3]));
                city.setPop(Double.parseDouble(record[4]));
                city.setCountry(record[5]);
                city.setIso2(record[6]);
                city.setIso3(record[7]);
                if (size > 8) {
                    city.setProvince(record[8]);
                }
                cities.add(city);
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        return cities;
    }
}
