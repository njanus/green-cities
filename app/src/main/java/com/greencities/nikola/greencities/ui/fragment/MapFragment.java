package com.greencities.nikola.greencities.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.greencities.nikola.greencities.R;
import com.greencities.nikola.greencities.model.City;

import java.io.Serializable;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static final String CITY_KEY = "city_key";
    private SupportMapFragment mapFragment;
    private GoogleMap mGoogleMap;
    private View mView;
    private City city;

    public static MapFragment newInstance(City city) {
        MapFragment fragment = new MapFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(CITY_KEY, (Serializable) city);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.f_map, container, false);
        city = (City) getArguments().getSerializable(CITY_KEY);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);//remember getMap() is deprecated!
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            double latitude = 0;
            double longitude = 0;
            String name = "";
            String country = "";
            mGoogleMap = googleMap;
            if (city != null) {
                latitude = city.getLat();
                longitude = city.getLng();
                name = city.getCity();
                country = city.getCountry();

            }

            LatLng coordinate = new LatLng(latitude, longitude);
            mGoogleMap.addMarker(new MarkerOptions().position(coordinate).
                    title(name)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.mipmap.ic_launcher))
                    .snippet(country)).showInfoWindow();
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 16f));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapFragment.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapFragment.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapFragment.onDestroy();

    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapFragment.onLowMemory();

    }

}
