package com.greencities.nikola.greencities.viewholders;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.greencities.nikola.greencities.R;
import com.greencities.nikola.greencities.enums.Status;
import com.greencities.nikola.greencities.model.City;

public class CityItemViewHolder extends RecyclerView.ViewHolder {
    public ImageButton imgViewMap;
    public TextView textViewCityName;


    public CityItemViewHolder(View itemView) {
        super(itemView);

        textViewCityName = itemView.findViewById(R.id.textViewCityName);
        imgViewMap = itemView.findViewById(R.id.imageButtonMap);

    }

    public void setCityData(City city) {
        textViewCityName.setText(city.getCity());
        if (city.getStatus() == Status.GREEN.getValue()) {
            textViewCityName.setTextColor(Color.WHITE);
            textViewCityName.setBackgroundResource(R.color.colorPrimary);
        } else {
            textViewCityName.setTextColor(Color.BLACK);
            textViewCityName.setBackgroundResource(R.color.white);

        }

    }
}
