package com.greencities.nikola.greencities.enums;

public enum Status {
    UNDEFINED(0),
    GREEN(1);

    private final int value;

    private Status(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
