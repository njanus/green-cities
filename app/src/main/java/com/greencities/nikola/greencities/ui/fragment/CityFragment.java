package com.greencities.nikola.greencities.ui.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.greencities.nikola.greencities.R;
import com.greencities.nikola.greencities.adapter.CityListAdapter;
import com.greencities.nikola.greencities.model.City;
import com.greencities.nikola.greencities.room.database.AppDatabase;

import java.util.ArrayList;
import java.util.List;

public class CityFragment extends Fragment implements CityListAdapter.CityAdapterListener {
    private static final String CITIES_KEY = "cities_key";
    private RecyclerView recyclerView;
    private TextView textViewNoResult;
    private CityListAdapter adapter;
    private List<City> cities = new ArrayList<>();
    private MenuItem menuItemSearch;
    private SearchView searchView;
    private CityFragmentListener cityFragmentListener;
    private CityListAdapter.CityAdapterListener cityAdapterListener;

    public static CityFragment newInstance() {
        CityFragment fragment = new CityFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    public void refreshList(int pos, boolean isGreen) {
        City city = cities.get(pos);
        if (isGreen) {
            city.setStatus(1);
        } else {
            city.setStatus(0);
        }
        adapter.notifyItemChanged(pos);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list_of_cities, container, false);
        textViewNoResult = view.findViewById(R.id.textViewNoSearchResult);
        recyclerView = view.findViewById(R.id.recyclerViewListOfPeople);
        cityAdapterListener = this;
        setHasOptionsMenu(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        new LoadCityDataTask().execute();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            cityFragmentListener = (CityFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement CityFragmentListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main, menu);
        menuItemSearch = menu.findItem(R.id.action_search);
        menuItemSearch.setVisible(true);
        searchView = (SearchView) menuItemSearch.getActionView();
        searchView.setQueryHint(this.getString(R.string.search_city));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.getFilter().filter("");
                return false;
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void toggleVisibility(Boolean searchFounded) {
        if (searchFounded) {
            textViewNoResult.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            textViewNoResult.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

    }

    @Override
    public void onMapChosen(City city) {
        if (cityFragmentListener != null) {
            cityFragmentListener.onMapChosen(city);
        }

    }

    @Override
    public void onCityDetailsChosen(City city, int position) {
        if (cityFragmentListener != null) {
            cityFragmentListener.onCityDetailsChosen(city, position);
        }

    }

    public interface CityFragmentListener {
        void onMapChosen(City city);

        void onCityDetailsChosen(City city, int pos);
    }

    private void loadCitiesFromDb() {
        cities = AppDatabase.getAppDatabase(getContext()).cityDao().getAllCities();
    }

    private class LoadCityDataTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                loadCitiesFromDb();
            } catch (Exception ignored) {
                Log.i("===", ignored.getMessage());
            }

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            adapter = new CityListAdapter(getContext(), cities, cityAdapterListener);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onPreExecute() {
            //  showProgressIndicator();
        }

    }
}

