package com.greencities.nikola.greencities.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.greencities.nikola.greencities.R;
import com.greencities.nikola.greencities.model.City;
import com.greencities.nikola.greencities.viewholders.CityItemViewHolder;

import java.util.ArrayList;
import java.util.List;

public class CityListAdapter extends RecyclerView.Adapter<CityItemViewHolder> implements Filterable {

    private LayoutInflater inflater;
    private Context context;
    private City city;
    private CityAdapterListener listener;
    private List<City> cities;
    private List<City> filteredCities = new ArrayList<>();

    public CityListAdapter(Context context, List<City> cities, CityAdapterListener listener) {
        this.context = context;
        this.cities = cities;
        this.filteredCities = cities;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CityItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_item, parent, false);
        return new CityItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CityItemViewHolder holder, final int position) {
        final City city = filteredCities.get(position);
        holder.setCityData(city);
        holder.imgViewMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onMapChosen(city);
                }
            }
        });
        holder.textViewCityName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onCityDetailsChosen(city, position);
                }
            }
        });
    }

    public void delete(int position) { //removes the row
        filteredCities.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return filteredCities.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String query = charSequence.toString();
                List<City> filtered = new ArrayList<>();
                if (query.isEmpty()) {
                    filtered = cities;
                } else if (query.length() >= 3) {
                    for (City city : cities) {
                        if (city.getCity().toLowerCase().contains(query.toLowerCase())) {
                            filtered.add(city);
                        }
                    }
                } else {
                    filtered = cities;
                }
                FilterResults results = new FilterResults();
                results.count = filtered.size();
                results.values = filtered;
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                if (results.count > 0) {
                    filteredCities = (List<City>) results.values;
                    if (listener != null) {
                        listener.toggleVisibility(true);
                    }
                } else if (listener != null) {
                    listener.toggleVisibility(false);
                }
                notifyDataSetChanged();

            }
        };
    }

    public interface CityAdapterListener {
        void toggleVisibility(Boolean searchFounded);

        void onMapChosen(City city);

        void onCityDetailsChosen(City city, int position);
    }
}

