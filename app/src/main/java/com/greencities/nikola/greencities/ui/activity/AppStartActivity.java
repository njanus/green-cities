package com.greencities.nikola.greencities.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.greencities.nikola.greencities.settings.SettingKeys;
import com.greencities.nikola.greencities.settings.Settings;

/**
 * This activity is invisible and is only used to decide which activity to start next
 */
public class AppStartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startNextActivity();
        finish();
    }

    private void startNextActivity() {
        if (!isFirstTimeAppStarted()) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            startActivity(new Intent(this, SplashScreenActivity.class));
        }
    }

    private boolean isFirstTimeAppStarted() {
        return Settings.getBoolean(SettingKeys.FIRST_TIME_APP_STARTED, true);
    }


}
