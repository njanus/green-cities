package com.greencities.nikola.greencities.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.greencities.nikola.greencities.R;
import com.greencities.nikola.greencities.csv.CSVFile;
import com.greencities.nikola.greencities.model.City;
import com.greencities.nikola.greencities.room.database.AppDatabase;
import com.greencities.nikola.greencities.settings.SettingKeys;
import com.greencities.nikola.greencities.settings.Settings;
import com.greencities.nikola.greencities.ui.fragment.CityFragment;
import com.greencities.nikola.greencities.ui.fragment.MapFragment;

import java.io.InputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, CityFragment.CityFragmentListener {

    private static final String TAG = "MainActivity";
    private static final int PICK_CITY_REQUEST = 1;
    private static final String CITY_DATA = "city_data";
    private List<City> cities;
    private ProgressBar loadingBar;
    private int progressStatus = 0;
    private Fragment fragment;
    private int position;
    private ProgressBar progressBarLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Settings.setBoolean(SettingKeys.FIRST_TIME_APP_STARTED, false);
        initView();
        new GetCityDataTask().execute();
    }

    private void initView() {
        loadingBar = findViewById(R.id.progressBarCity);
        progressBarLine = findViewById(R.id.progressBarLine);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void getCityData() {
        InputStream inputStream = getResources().openRawResource(R.raw.cities);
        CSVFile csvFile = new CSVFile(inputStream);
        cities = csvFile.readDataFromCSV(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
            finish();
            Intent camera = new Intent("android.media.action.IMAGE_CAPTURE");
            startActivity(camera);
        } else if (id == R.id.nav_all_cities) {
            setInitFragment();

        } else if (id == R.id.nav_share) {
            // share app with other user

        } else if (id == R.id.nav_send) {
            // send mail app with other user
        } else if (id == R.id.nav_exit) {
            // send mail app with other user
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showProgressIndicator() {
        if (loadingBar != null && progressBarLine != null) {
            loadingBar.setVisibility(View.VISIBLE);
            progressBarLine.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgressIndicator() {
        if (loadingBar != null && progressBarLine != null) {
            loadingBar.setVisibility(View.GONE);
            progressBarLine.setVisibility(View.GONE);
        }
    }

    private void setInitFragment() {
        fragment = null;
        try {
            fragment = CityFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frameLayoutContent, fragment).commitAllowingStateLoss();
    }

    @Override
    public void onMapChosen(City city) {
        fragment = null;
        try {
            fragment = MapFragment.newInstance(city);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frameLayoutContent, fragment).commitAllowingStateLoss();

    }

    @Override
    public void onCityDetailsChosen(City city, int pos) {
        position = pos;
        Intent pickCityIntent = new Intent(this, CityDetailsActivity.class);
        pickCityIntent.putExtra(CITY_DATA, city);
        startActivityForResult(pickCityIntent, PICK_CITY_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == PICK_CITY_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                boolean isGreen = data.getExtras().getBoolean("result");
                if (fragment instanceof CityFragment) {
                    ((CityFragment) fragment).refreshList(position, isGreen);
                }
            }
        }
    }

    private class GetCityDataTask extends AsyncTask<String, Integer, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            try {
                getCityData();
                for (City city : cities) {
                    AppDatabase.getAppDatabase(MainActivity.this).cityDao().insertCity(city);
                    progressStatus++;
                    if (progressStatus % (cities.size() / 25) == 0) {
                        publishProgress(progressStatus * 100 / cities.size());
                    }
                }


            } catch (Exception ignored) {
                Log.i("===", ignored.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            hideProgressIndicator();
            setInitFragment();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBarLine.setProgress(values[0]);
        }

        @Override
        protected void onPreExecute() {
            showProgressIndicator();
            progressBarLine.setMax(100);

        }

    }
}
