package com.greencities.nikola.greencities.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import java.util.Set;

public class Settings {

    private static Settings instance = new Settings();

    private SharedPreferences mSharedPreferences;

    private Settings() {
    }

    public static void initialize(@NonNull Context context) {
        if (instance.mSharedPreferences == null) {
            instance.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    public static void setBoolean(@SettingKeys.SettingKey final String key, final boolean value) {
        instance.setSettings(new SettingsModifier() {
            @Override
            public void modify(SharedPreferences.Editor editor) {
                editor.putBoolean(key, value);
            }
        });
    }

    public static boolean getBoolean(@SettingKeys.SettingKey final String key, final boolean defaultValue) {
        return instance.mSharedPreferences.getBoolean(key, defaultValue);
    }

    public static void setInt(@SettingKeys.SettingKey final String key, final int value) {
        instance.setSettings(new SettingsModifier() {
            @Override
            public void modify(SharedPreferences.Editor editor) {
                editor.putInt(key, value);
            }
        });
    }

    public static int getInt(@SettingKeys.SettingKey final String key, final int defaultValue) {
        return instance.mSharedPreferences.getInt(key, defaultValue);
    }

    public static void setLong(@SettingKeys.SettingKey final String key, final long value) {
        instance.setSettings(new SettingsModifier() {
            @Override
            public void modify(SharedPreferences.Editor editor) {
                editor.putLong(key, value);
            }
        });
    }

    public static long getLong(@SettingKeys.SettingKey final String key, final long defaultValue) {
        return instance.mSharedPreferences.getLong(key, defaultValue);
    }

    public static void setFloat(@SettingKeys.SettingKey final String key, final float value) {
        instance.setSettings(new SettingsModifier() {
            @Override
            public void modify(SharedPreferences.Editor editor) {
                editor.putFloat(key, value);
            }
        });
    }

    public static float getFloat(@SettingKeys.SettingKey final String key, final float defaultValue) {
        return instance.mSharedPreferences.getFloat(key, defaultValue);
    }

    public static void setString(@SettingKeys.SettingKey final String key, final String value) {
        instance.setSettings(new SettingsModifier() {
            @Override
            public void modify(SharedPreferences.Editor editor) {
                editor.putString(key, value);
            }
        });
    }

    public static String getString(@SettingKeys.SettingKey final String key, final String defaultValue) {
        return instance.mSharedPreferences.getString(key, defaultValue);
    }

    public static void setStringSet(@SettingKeys.SettingKey final String key, final Set<String> value) {
        instance.setSettings(new SettingsModifier() {
            @Override
            public void modify(SharedPreferences.Editor editor) {
                editor.putStringSet(key, value);
            }
        });
    }

    public static Set<String> getStringSet(@SettingKeys.SettingKey final String key, final Set<String> defaultValues) {
        return instance.mSharedPreferences.getStringSet(key, defaultValues);
    }

    public static void remove(@SettingKeys.SettingKey final String... keys) {
        SharedPreferences.Editor editor = instance.mSharedPreferences.edit();

        for (String key : keys) {
            editor.remove(key);
        }

        editor.apply();
    }

    public static void clearSharedPrefs() {
        instance.mSharedPreferences.edit().clear().apply();
    }

    private void setSettings(SettingsModifier settingsModifier) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        settingsModifier.modify(editor);
        editor.apply();
    }

    /**
     * The {@link SettingsModifier} functional interface provides the means
     * to set several settings at once using the provided {@link SharedPreferences.Editor}
     */
    private interface SettingsModifier {

        void modify(SharedPreferences.Editor editor);
    }
}
